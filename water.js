/**
 * @file water.js
 * @author lili39@didichuxing.com
 * @date 2018-07-07 12:19:05
 */

const md5 = require('md5');

const request = require('./request');
const config = require('./config');

const grabHouse = roomId => {
    return new Promise((resolve, reject) => {
        request
            .get(
                `https://house-on-sale.focus.cn/subscribe/${
                    config.PROJECT_ID
                }/buildingRooms`
            )
            .then(data => {
                let userSubscribeTimes = data.match(
                    /window.userSubscribeTimes = "(.+?)"/i
                );

                let userId = data.match(/data-userId="(.+?)">/i);

                if (userId) {
                    userId = userId[1];
                } else {
                    userId = config.USER_ID;
                }

                if (userSubscribeTimes) {
                    const sign = md5(
                        `${userId}${
                            config.PROJECT_ID
                        }${roomId}BzMAfignvB8NEso${userSubscribeTimes[1] || 0}`
                    );
                    // console.log(sign, userSubscribeTimes[1]);
                    const subscribeData = {
                        userId,
                        roomId,
                        projectId: config.PROJECT_ID,
                        sign
                    };
                    // console(subscribeData);
                    request
                        .post(
                            'https://house-on-sale.focus.cn/ajax/subscribe',
                            subscribeData
                        )
                        .then(data => {
                            resolve(data);
                        });
                } else {
                    resolve(false);
                }
            });
    });
};

// grabHouse(122711);

const qiang = res => {
    // console.log(config.ROOM_ID_LIST_TMP);
    if (config.ROOM_ID_LIST_TMP.length) {
        let roomId = config.ROOM_ID_LIST_TMP.splice(0, 1)[0];
        grabHouse(roomId).then(data => {
            // console.log(roomId, data);
            if (data) {
                // console.log(roomId, data);
                if (+data.code === 200) {
                    res.send(data.data);
                } else if (data.msg.match('开盘活动未开始')) {
                    res.send(data.msg);
                } else {
                    if (+data.code === 4001) {
                        res.send(data.msg);
                        return;
                    } else if (+data.code === 1) {
                        config.ROOM_ID_LIST.splice(
                            config.ROOM_ID_LIST.findIndex(id => id === roomId),
                            1
                        );
                    }
                    qiang(res);
                }
            } else {
                res.send('cookie失效');
            }
        });
    } else {
        res.send('没抢到!');
    }
};

// qiang();
module.exports = qiang;
