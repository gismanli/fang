/**
 * @file filter.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 13:29:04
 */

const fs = require('fs');

const xlsx = require('node-xlsx');
// const XLSX = require('xlsx');

const path = __dirname + '/.tmp/room/';

const files = fs.readdirSync(path);

let roomList = [];

files.map(file => {
    let data = JSON.parse(fs.readFileSync(path + file, 'utf-8'));

    let roomInfo = data.data.saleRoom;
    let unit = roomInfo.buildingId - 630 + '号楼';
    if (+roomInfo.unitId === 1116) {
        unit = roomInfo.buildingId - 630 + '号楼2单元';
    }

    roomList.push([
        unit,
        +roomInfo.number,
        +roomInfo.collectNum,
        +roomInfo.area,
        parseFloat(roomInfo.totalPriceDouble),
        parseFloat(roomInfo.totalPriceDouble) / +roomInfo.area,
        parseFloat(roomInfo.totalPriceDouble) * 0.4,
        +roomInfo.roomId
    ]);
});

roomList = roomList.sort((a, b) => {
    return a[2] - b[2];
    // return a[3] - b[3];
});

roomList.unshift([
    '楼号',
    '户号',
    '收藏次数',
    '面积',
    '具体总价',
    '均价',
    '首付',
    'ID'
]);

let buffer = xlsx.build([
    {
        name: '华润理想国',
        data: roomList
    }
]);

// fs.writeFileSync(`./result/room.xlsx`, buffer);
fs.writeFileSync('./room_info.xlsx', buffer);
