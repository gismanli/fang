/**
 * @file index.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 11:26:29
 */

const express = require('express');

const shell = require('shelljs');
// const XLSX = require('xlsx');
// const TableBuilder = require('table-builder');

const water = require('./water');
const config = require('./config');
const request = require('./request');

const app = express();

app.get('/qiang', function(req, res) {
    config.ROOM_ID_LIST_TMP = config.ROOM_ID_LIST.slice();
    water(res);
});

app.get('/qiangs', function(req, res) {
    config.ROOM_ID_LIST_TMP = config.ROOM_WANT_LIST.slice();
    water(res);
});

app.get('/login', function(req, res) {
    request.auth().then(() => {
        res.send('获取cookie成功');
    });
});

app.use('/file', express.static('result'));

const server = app.listen(3000, function() {
    let host = server.address().address;
    let port = server.address().port;

    console.log('app listening at http://%s:%s', host, port);
});
