/**
 * @file room.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 10:59:06
 */

'use strict';

const fs = require('fs');

const config = require('./config');

const request = require('./request');

// Promise.all()

config.BUILDING_ID_LIST.map(buildingId => {
    return request
        .get('https://house-on-sale.focus.cn/ajax/getUnitRooms', {
            userId: config.USER_ID,
            buildingId,
            projectId: config.PROJECT_ID,
            unitId: null,
            filterSelected: 0
        })
        .then(res => {
            fs.writeFileSync(
                `./.tmp/floor/${buildingId - 630}号楼.json`,
                JSON.stringify(res.data)
            );
        });
});
