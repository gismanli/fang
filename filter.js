/**
 * @file filter.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 13:29:04
 */

const fs = require('fs');

const xlsx = require('node-xlsx');
// const XLSX = require('xlsx');

const path = __dirname + '/.tmp/floor/';

const files = fs.readdirSync(path);

let roomList = [];
let roomIdList = [];

files.map(file => {
    let data = JSON.parse(fs.readFileSync(path + file, 'utf-8'));

    let floorData = data.data.unitShowData.floorShowDataList;
    let unit = data.data.unitShowData.unit;

    floorData.map(floor => {
        let floorName = floor.floor.floorName;
        floor.roomList.map(room => {
            let lou = file.replace('.json', '');
            if (
                room.layout &&
                room.number !== '不可售' &&
                // parseInt(lou) !== 1 &&
                // parseInt(lou) !== 4 &&
                // parseInt(lou) !== 7 &&
                // parseInt(lou) !== 11 &&
                (parseInt(lou) === 12) &&
                (room.number.match('01')) &&
                // +room.area < 92 &&
                // room.layout.layoutName.length === 3 &&
                // (room.layout.layoutName === 'C户型' ||
                //     room.layout.layoutName === 'D户型') &&
                +floorName > 9 && 
                +floorName !== 14 &&
                +floorName !== 13 &&
                +floorName !== unit.totalFloor
            ) {
                // 不要14层 // 不要顶层
                roomList.push([
                    file.replace('.json', ''),
                    floorName,
                    +room.number,
                    +room.collectNum,
                    room.layout.layoutName,
                    +room.area,
                    room.totalPrice,
                    parseFloat(room.totalPriceDouble),
                    parseFloat(room.totalPriceDouble) / +room.area,
                    parseFloat(room.totalPriceDouble) * 0.4,
                    +room.roomId
                ]);
                // roomIdList.push(+room.roomId);
            }
        });
    });
});

roomList = roomList.sort((a, b) => {
    // return a[7] - b[7];
    return b[3] - a[3];
});

roomIdList = roomList.map(item => item[10]);

// fs.writeFileSync('./.tmp/room-id-list.json', JSON.stringify(roomIdList));
fs.writeFileSync('./.tmp/room-script-id-list.json', JSON.stringify(roomIdList));

roomList.unshift([
    '楼号',
    '楼层',
    '户号',
    '收藏次数',
    '户型',
    '面积',
    '总价',
    '具体总价',
    '均价',
    '首付',
    'ID'
]);

let buffer = xlsx.build([
    {
        name: '华润理想国',
        data: roomList
    }
]);

// fs.writeFileSync(`./result/room.xlsx`, buffer);
fs.writeFileSync(`./result/room_${new Date().getTime()}.xlsx`, buffer);
