/**
 * @file room.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 10:59:06
 */

'use strict';

const fs = require('fs');

const config = require('./config');

const request = require('./request');

let data = JSON.parse(
    fs.readFileSync(__dirname + '/.tmp/room-id-list.json', 'utf-8')
);

const digui = () => {
    let num = data.splice(0, 1);
    if (num.length) {
        return request
            .get('https://house-on-sale.focus.cn/ajax/getRoomInfo', {
                roomId: num[0],
                projectId: config.PROJECT_ID
            })
            .then(res => {
                fs.writeFileSync(
                    `./.tmp/room/${num[0]}.json`,
                    JSON.stringify(res.data)
                );
                digui();
            });
    }
};

digui();
