/**
 * @file login.js
 * @author lili39@didichuxing.com
 * @date 2018-07-06 11:01:41
 */

'use strict';

const axios = require('axios');
const md5 = require('md5');
const qs = require('qs');
const fs = require('fs');

const config = require('./config');

// const data = {
//     captcha: null,
//     imgcode: null,
//     mobile: MOBILE,
//     name: NAME,
//     noNeedTelLogin: 'true',
//     projectId: '4735782768'
// };

const loginData = {
    phone: config.MOBILE,
    realName: config.NAME,
    imgcode: null,
    captcha: null,
    projectId: 4735782768,
    sign: md5(config.MOBILE + 'BzMAfignvB8NEso')
};

const loginUrl = 'https://house-on-sale.focus.cn/ajax/userNoCaptchaCheckLogin';

// console.log(loginUrl, loginData);

const login = () => {
    return axios.post(loginUrl, qs.stringify(loginData)).then(res => {
        // console.log(res.data);
        let setCookie = res.headers['set-cookie'];
        let cookies = '';
        setCookie.map(cookie => {
            cookies += cookie.split(';')[0] + ';';
        });
        fs.writeFileSync('./.cookie', cookies);
        return cookies;
    });
};

// login()

module.exports = login;
