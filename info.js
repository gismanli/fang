/**
 * @file info.js
 * @author lili39@didichuxing.com
 * @date 2018-07-09 21:09:06
 */

const config = require('./config');
const request = require('./request');

// let list = config.ROOM_ID_LIST;
let list = config.ROOM_WANT_LIST;

list.map(id => {
    request
        .get('https://house-on-sale.focus.cn/ajax/getRoomInfo', {
            roomId: id,
            projectId: config.PROJECT_ID
        })
        .then(data => {
            if (!data) {
                return;
            }
            let {
                collectNum,
                roomId,
                buildingId,
                number,
                layoutId,
                totalPrice
            } = data.data.saleRoom;
            console.log(
                `${buildingId - 630}号楼-${number}-${
                    layoutId - 814 ? 'D' : 'C'
                }户型-${roomId}-收藏${collectNum}次-总价${totalPrice}`
            );
        });
});
